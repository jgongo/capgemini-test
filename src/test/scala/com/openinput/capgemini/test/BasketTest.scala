package com.openinput.capgemini.test

import com.openinput.capgemini.test.Warehouse.{Apple, Banana, Orange}
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{Matchers, WordSpec}

/*
 * capgemini-test
 * Created by jgonzalez on 21/3/17 14:20
 * Copyright © 2017 Open Technology Network, S.L. All rights reserved
 */
class BasketTest extends WordSpec with Matchers with GeneratorDrivenPropertyChecks {
  "A basket" when {
    "empty" should {
      "give a total amount of 0" in {
        Basket(Nil).total should be(0)
      }
    }

    "non empty" should {
      "properly compute total" in {
        forAll { basket: Basket =>
          basket.total should be(basket.items.map(Warehouse.items(_).price).sum)
        }
      }

    }

    "only containing apples" when {
      "in pairs" should {
        "properly compute total with offers" in {
          forAll(appleBaskets) { basket: Basket =>
            val basketSize = basket.items.size
            whenever(basketSize % 2 == 0) { basket.totalWithOffers should be (Apple.price * basketSize / 2) }
          }
        }
      }

      "not in pairs" should {
        "properly compute total with offers" in {
          forAll(appleBaskets) { basket: Basket =>
            val basketSize = basket.items.size
            whenever(basketSize % 2 != 0) { basket.totalWithOffers should be (Apple.price * (1 + basketSize / 2)) }
          }
        }
      }
    }

    "only containing oranges" when {
      "in trios" should {
        "properly compute total with offers" in {
          forAll(orangeBaskets) { basket: Basket =>
            val basketSize = basket.items.size
            whenever(basketSize % 3 == 0) { basket.totalWithOffers should be (2 * Orange.price * (basketSize / 3)) }
          }
        }
      }

      "not in trios" should {
        "properly compute total with offers" in {
          forAll(orangeBaskets) { basket: Basket =>
            val basketSize = basket.items.size
            whenever(basketSize % 3 != 0) { basket.totalWithOffers should be (2 * Orange.price * (basketSize / 3) + Orange.price * (basketSize % 3)) }
          }
        }
      }
    }

    "only containing apples and bananas" when {
      "there are more apples than bananas" should {
        "properly compute total with offers" in {
          forAll(appleAndBananasBaskets) { basket: Basket =>
            val numberOfApples = basket.items.count(_ == "apple")
            val numberOfBananas = basket.items.count(_ == "banana")

            whenever(numberOfApples >= numberOfBananas) {
              val remainingApples = numberOfApples - numberOfBananas
              basket.totalWithOffers should be ((numberOfBananas + remainingApples / 2 + remainingApples % 2) * Apple.price)
            }
          }
        }
      }

      "there are less apples than bananas" should {
        "properly compute total with offers" in {
          forAll(appleAndBananasBaskets) { basket: Basket =>
            val numberOfApples = basket.items.count(_ == "apple")
            val numberOfBananas = basket.items.count(_ == "banana")

            whenever(numberOfApples < numberOfBananas) {
              val remainingBananas = numberOfBananas - numberOfApples
              basket.totalWithOffers should be (numberOfApples * Apple.price + Banana.price * (remainingBananas / 2 + remainingBananas % 2))
            }
          }
        }
      }
    }
  }
}
