package com.openinput.capgemini

import com.openinput.capgemini.test.Warehouse.{Apple, Barcode, Orange}
import org.scalacheck.{Arbitrary, Gen}

/*
 * capgemini-test
 * Created by jgonzalez on 21/3/17 14:52
 * Copyright © 2017 Open Technology Network, S.L. All rights reserved
 */
package object test {
  private val barcodes = Warehouse.items.keySet.toList

  implicit def baskets: Arbitrary[Basket] = Arbitrary {
    for {
      items <- Gen.containerOf[List, Barcode](Gen.oneOf(barcodes))
    } yield Basket(items)
  }

  def appleBaskets: Gen[Basket] = for {
    items <- Gen.containerOf[List, Barcode](Gen.const("apple"))
  } yield Basket(items)

  def appleAndBananasBaskets: Gen[Basket] = for {
    items <- Gen.containerOf[List, Barcode](Gen.oneOf("apple", "banana"))
  } yield Basket(items)

  def orangeBaskets: Gen[Basket] = for {
    items <- Gen.containerOf[List, Barcode](Gen.const("orange"))
  } yield Basket(items)
}
