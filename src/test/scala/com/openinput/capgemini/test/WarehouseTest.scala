package com.openinput.capgemini.test

import com.openinput.capgemini.test.Warehouse.{Apple, Banana, Orange}
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{Matchers, WordSpec}

/*
 * capgemini-test
 * Created by jgonzalez on 21/3/17 13:33
 * Copyright © 2017 Open Technology Network, S.L. All rights reserved
 */
class WarehouseTest extends WordSpec with Matchers with GeneratorDrivenPropertyChecks {
  "The warehouse" should {
    "contain fruits with the correct barcode" in {
      Warehouse.items should contain("apple" -> Apple)
      Warehouse.items should contain("orange" -> Orange)
      Warehouse.items should contain("banana" -> Banana)
    }

    "not contain any other fruit" in {
      // This in fact doesn't work, as the probability of finding the name of a fruit included in the warehouse
      // using property checking is quite low. A pity the property definition is crystal clear
      forAll { fruit: String =>
        whenever(fruit != "apple" && fruit != "orange") {
          Warehouse.items shouldNot contain(fruit)
        }
      }
      // So instead we use this
      Warehouse.items should have size (3)
    }

    "have correct names for each fruit" in {
      Warehouse.items("apple").name should be("Apple")
      Warehouse.items("orange").name should be("Orange")
      Warehouse.items("banana").name should be("Banana")
    }

    "have correct prices for each fruit" in {
      Warehouse.items("apple").price should be(BigDecimal("0.60"))
      Warehouse.items("orange").price should be(BigDecimal("0.25"))
      Warehouse.items("banana").price should be(BigDecimal("0.20"))
    }
  }
}
