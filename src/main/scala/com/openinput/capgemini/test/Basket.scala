package com.openinput.capgemini.test

import com.openinput.capgemini.test.Warehouse.{Apple, Banana, Barcode, Orange}

/*
 * capgemini-test
 * Created by jgonzalez on 21/3/17 14:13
 * Copyright © 2017 Open Technology Network, S.L. All rights reserved
 */
// A basket is just a list of barcodes
case class Basket(items: List[Barcode]) {
  def total = items.map(Warehouse.items(_).price).sum
  // This is not extensible but it's the quickest way of getting it done. A proper way of doing this
  // would be for example to define packs as a subclass of Item, so we can traverse the item list and group
  // items in packs
  def totalWithOffers = {
    val noFruit = Item("nothing", 0)
    val fruits = items.map(Warehouse.items)

    val apples = fruits.filter(_ == Apple)
    val bananas = fruits.filter(_ == Banana)
    val oranges = fruits.filter(_ == Orange)
    val numberOfOranges = oranges.count(_ == Orange)

    val (appleBananaPacks, remainingFruitPacks) = apples.zipAll(bananas, noFruit, noFruit).span {
      case (fruit1, fruit2) => fruit1 != noFruit && fruit2 != noFruit
    }

    val remainingFruits = remainingFruitPacks.map {
      case (fruit1, fruit2) => if (fruit1 == noFruit) fruit2 else fruit1
    }

    val remainingFruitsValue = if (remainingFruits.isEmpty) BigDecimal(0) else (remainingFruits.size / 2 + remainingFruits.size % 2) * remainingFruits.head.price
    appleBananaPacks.size * Apple.price + remainingFruitsValue + (numberOfOranges - numberOfOranges / 3) * Orange.price
  }
}
