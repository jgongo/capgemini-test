package com.openinput.capgemini.test

/*
 * capgemini-test
 * Created by jgonzalez on 21/3/17 13:21
 * Copyright © 2017 Open Technology Network, S.L. All rights reserved
 */
// Class to represent a sellable item.
// For the sake of simplicity I'm using Double for prices although this is
// bad practice due to rounding problems related with floating point arithmetic
case class Item(name: String, price: BigDecimal)

// We have a warehouse where we keep a list of products with their corresponding barcodes
object Warehouse {
  // Just creating this here for convenience, this could be read from a file or database
  object Apple extends Item("Apple", BigDecimal("0.60"))
  object Orange extends Item("Orange", BigDecimal("0.25"))
  object Banana extends Item("Banana", BigDecimal("0.20"))


  type Barcode = String
  val items: Map[Barcode, Item] = Map("apple" -> Apple, "orange" -> Orange, "banana" -> Banana)
}
