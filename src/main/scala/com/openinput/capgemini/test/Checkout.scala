package com.openinput.capgemini.test

import java.text.NumberFormat
import java.util.Locale

/*
 * capgemini-test
 * Created by jgonzalez on 21/3/17 16:54
 * Copyright © 2017 Open Technology Network, S.L. All rights reserved
 */
object Checkout extends App {
  val validBarcodes = args filter (Warehouse.items.keySet.contains(_))
  if (args.length != validBarcodes.length) {
    println(s"Invalid barcodes found. Valid barcodes are: ${Warehouse.items.keySet.mkString(" ")}")
  }

  val formatter = NumberFormat.getCurrencyInstance(Locale.UK) // Forced to display pounds
  val basket = Basket(validBarcodes.toList)
  println(s"You bought $basket")
  println(s"Total: ${formatter.format(basket.total)}")
  println(s"Total (with offers): ${formatter.format(basket.totalWithOffers)}")
}
