name := "capgemini-test"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
  // Testing
  "org.scalatest"  %% "scalatest"    % "3.0.1"  % Test,
  "org.scalacheck" %% "scalacheck"   % "1.13.4" % Test
)
